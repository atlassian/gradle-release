# Moved to GitHub

This module migrated to https://github.com/atlassian-labs/gradle-release due to [JPERF-720](https://ecosystem.atlassian.net/browse/JPERF-720).
